class PiplineTableWeek
  has_one :potential_loss
  has_one :new_lead
  has_one :trial_lead
  has_one :offer_sent_lead
  has_one :closed_lead
end 

class Create_pipline_table_weeks < ActiveRecord::Migration
  def change
    create_table :pipline_table_weeks do |t|
      t.integer :potential_loss_id
      t.integer :new_id
      t.integer :trial_id
      t.integer :offer_sent_id
      t.integer :closed_id
      t.integer :period
      t.integer :sort
    end
  end
end

class PotentialLoss
  belongs_to :pipline_table_week
end 

class Create_potential_loss < ActiveRecord::Migration
  def change
    create_table :potential_loss do |t|
      t.string  :name
      t.string  :avatar
      t.string  :email
      t.date    :lead_created_at
      t.integer :study_histories_last_id
      t.string  :klass
      t.integer :pipline_table_week_id
    end
  end
end

class NewLead
  belongs_to :pipline_table_week
end

class Create_new_leads < ActiveRecord::Migration
  def change
    create_table :potential_loss do |t|
      t.string  :name
      t.string  :avatar
      t.string  :email
      t.date    :lead_created_at
      t.string  :klass
      t.integer :pipline_table_week_id
    end
  end
end

class TrialLead
  belongs_to :pipline_table_week
end

class Create_trial_leads < ActiveRecord::Migration
  def change
    create_table :potential_loss do |t|
      t.string  :name
      t.string  :avatar
      t.string  :email
      t.date    :lead_created_at
      t.string  :klass
      t.string  :state
      t.integer :pipline_table_week_id
    end
  end
end

class OfferSentLead
  belongs_to :pipline_table_week
end

class Create_offer_sent_leads < ActiveRecord::Migration
  def change
    create_table :potential_loss do |t|
      t.string  :name
      t.string  :avatar
      t.string  :email
      t.date    :lead_created_at
      t.string  :klass
      t.string  :state
      t.integer :pipline_table_week_id
      t.float   :potential
    end
  end
end

class ClosedLead
  belongs_to :pipline_table_week
end

class Create_closed_leads < ActiveRecord::Migration
  def change
    create_table :potential_loss do |t|
      t.string  :name
      t.string  :avatar
      t.string  :email
      t.date    :lead_created_at
      t.string  :klass
      t.string  :state
      t.integer :pipline_table_week_id
      t.integer :won_total
      t.integer :lost_potential
      t.integer :potential_count
    end
  end
end

class Period < ActiveRecord::Base
  has_many :pipilene_leads
  has_one  :timeline_metrics
end


class TimelineMetric < ActiveRecord::Base
  belongs_to :period
end

class TimelineMetrics < ActiveRecord::Migration
  def change
    create_table :timeline_metrics do |t|
      t.integer :tracks
      t.integer :leads
      t.integer :won
      t.float   :tracks_to_leads
      t.float   :leads_to_wons
      t.float   :tracks_to_wons
      t.integer :lost
      t.integer :left_opened_caunt
      t.float   :payments
      t.float   :withdrawals_rub
      t.float   :withdrawals_eur
      t.float   :withdrawals_usd
      t.integer :active_students_count
      t.integer :completed_hours
      t.integer :average_lesson 
      t.integer :difference_tracks
      t.integer :difference_leads
      t.integer :difference_won
      t.float   :difference_tracks_to_leads
      t.float   :difference_leads_to_wons
      t.float   :difference_tracks_to_wons
      t.integer :difference_lost
      t.integer :difference_left_opened_caunt
      t.float   :difference_payments
      t.float   :difference_withdrawals_rub
      t.float   :difference_withdrawals_eur
      t.float   :difference_withdrawals_usd
      t.integer :difference_active_students_count
      t.integer :difference_completed_hours
      t.integer :difference_average_lesson 
    end
  end
end

class TimelineLead < ActiveRecord::Base
  belongs_to :period
end

class TimelineLeads < ActiveRecord::Migration
  def change
    create_table :timeline_leds do |t|
      t.string  :name
      t.string  :email
      t.string  :state
      t.integer :completed_lessons
      t.date    :course_started_at 
      t.integer :potential_count
      t.string  :avg
      t.integer :won_total
      t.integer :won_rub
      t.bolean  :lost
      t.bolean  :won
      t.bolean  :active_student
    end
  end
end